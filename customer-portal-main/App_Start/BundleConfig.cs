﻿using System.Web;
using System.Web.Optimization;

namespace CustomerPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.bundle.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/adminlte/plugins/fontawesome-free/css/all.min.css",
                      "~/adminlte/css/adminlte.min.css",
                      "~/adminlte/plugins/summernote/summernote-bs4.min.css",
                      "~/adminlte/plugins/fullcalendar/main.css",
                      "~/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css",
                      "~/adminlte/plugins/sweetalert2/sweetalert2.min.css",
                      "~/Content/site.css",
                      "~/adminlte/plugins/select2/css/select2.min.css",
                      "~/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"
                      ));
            
            bundles.Add(new ScriptBundle("~/adminlte/js").Include(
             "~/adminlte/js/adminlte.min.js",
             "~/adminlte/plugins/summernote/summernote-bs4.min.js",
             "~/adminlte/plugins/fullcalendar/main.js",
             "~/adminlte/plugins/datatables/jquery.dataTables.min.js",
             "~/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js",
             "~/adminlte/plugins/sweetalert2/sweetalert2.all.min.js",
             "~/Scripts/umd/jquery-3.6.0.min.js",
             "~/adminlte/plugins/moment/moment.min.js",
             "~/adminlte/plugins/popper/popper.min.js",
             "~/adminlte/plugins/select2/js/select2.min.js"
             ));
        }
    }
}
