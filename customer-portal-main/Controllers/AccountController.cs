﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCrypt.Net;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.IO;
using MimeKit;
using MimeKit.Text;
using System.Security.Cryptography;
using System.Text;
using CustomerPortal.Helper;
using CustomerPortal.Repository;
using System.Text.RegularExpressions;

namespace CustomerPortal.Controllers
{
    public class AccountController : Controller
    {
        //private readonly DBCustomerPortalEntities _context;
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();
        IGlobal _global = new GlobalHelper();

        // GET: Account
        
        public ActionResult Index()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie == null && Session["role"] == null)
            {
                return RedirectToAction("Login", "LoginPage");
            }
            if(NewCookie != null)
            {
                var role = _global.Decrypt(NewCookie.Values.Get("Role"));
                if(role != "admin")
                {
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.role = _global.Decrypt(NewCookie.Values.Get("Role"));
                ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
            }
            if(Session["role"] != null)
            {
                var role = Session["role"];
                if (role != "admin")
                {
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.role = Session["role"];
                ViewBag.value = Session["name"];
            }
            var ListCustIds = GetEpicorDataRepo.Instance.GetListCustID().ToList();
            var data = ListCustIds.Select(x => x.CustID).ToList();
            ViewBag.custid = data;
            return View();
        }
        [HttpPost]
        public ActionResult GetAllUser()
        {
            var datas = _context.MasterUsers.ToList();
            return Json(new
            {
                metaData = new { code = 200, message = "ALl Users Loaded Successfully" },
                response = datas
                //data = datas
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUser()
        {
            var listUser = _context.MasterUsers.ToList();
            return Json(new { data = listUser }, JsonRequestBehavior.AllowGet);
        }


        public MasterUser GetByID(string custId)
        {
            return _context.MasterUsers.Where(x => x.CustID == custId).FirstOrDefault();   
        }

        [HttpPost]
        public JsonResult AmbilUser(string custId)
        {
            //string custId = Request.Form.GetValues("custId").FirstOrDefault();
            var user = GetByID(custId);
            return Json(new
            {
                metaData = new { code = 200, message = "OK" },
                response = user
            });
        }

        //POST: Add New 
        [HttpPost]
        public ActionResult Create(MasterUser user)
        {
            var cekUserId = CekUserID(user.CustID);
            var CekDuplikasiEmai = CekEmailUser(user.Email);
            if (cekUserId || CekDuplikasiEmai)
            {
                var pesan = "";
                if (cekUserId)
                {
                    pesan = $"Customer ID {user.CustID} Already Exist!";
                }
                else
                {
                    pesan = $"Customer Email {user.Email} Already Exist!";
                }
                return Json(new
                {
                    metaData = new { code = 402, message = pesan },
                    response = ""
                });

            }
            //defaut password
            var pass = "String Sembarang";
            user.Password = BCrypt.Net.BCrypt.HashPassword(pass);
            user.CreatedAt = DateTime.Now;
            _context.MasterUsers.Add(user);
            _context.SaveChanges();
            return Json( new {
                metaData = new { code = 200, message = "New User Created Successfully" },
                response = user
            });
        }
        [HttpPost]
        public ActionResult Update(string custId, MasterUser user)
        {
            var cekUserId = CekUserID(custId);
            var selectedUser = GetByID(custId);
            selectedUser.CustID = user.CustID;
            selectedUser.Name = user.Name;
            selectedUser.Email = user.Email;
            selectedUser.UpdatedAt = DateTime.Now;
            _context.SaveChanges();
            return Json(new
            {
                metaData = new { code = 200, message = "User Updated Successfully" },
                response = user
            });
        }

        [HttpPost]
        public JsonResult Delete(string custId)
        {
            var user = GetByID(custId);
            _context.MasterUsers.Remove(user);
            _context.SaveChanges();
            return Json(new
            {
                metaData = new { code = 200, message = "New User Removed Successfully" },
                response = user
            });
        }

        [HttpPost]
        public JsonResult AktifkanUser(string custId, string status)
        {
            var SelectedUser = GetByID(custId);
            var BodyEmail = "";
            var password = "";
            var pesan = "";
            if(status == "false")
            {
                pesan = "User successfully activated";
                password = _global.GeneratePassword();
                BodyEmail = _global.GenerateBodyEmail(status, SelectedUser.Email, password,SelectedUser.CustID);
                SelectedUser.IsActive = true;
                SelectedUser.Password = BCrypt.Net.BCrypt.HashPassword(password);
            }
            else
            {
                pesan = "User successfully disabled";
                BodyEmail = _global.GenerateBodyEmail(status, SelectedUser.Email, password, SelectedUser.CustID);
                SelectedUser.IsActive = false;
                SelectedUser.Verified = false;
            }

             _global.KirimkanEmail("Test Kirim Email", BodyEmail, "anthonyheatubun@gmail.com");
            _context.SaveChanges();
           
            return Json(new
            {
                metaData = new { code = 200, message = pesan },
                response = ""
            });
        }

        //[HttpPost]
        //[Route("Verified/{custId}")]
        [HttpGet]
        public ActionResult Verified(string id)
        {
            var user = GetByID(_global.Decrypt(id));
            var result = CekUserID(_global.Decrypt(id));
            if (result)
            {
                ViewBag.result = "YES";
                user.Verified = true;
                _context.SaveChanges();
            }
            else
            {
              ViewBag.result = "NO";
            }
            return View();
        }

        private bool CekUserID(string userID)
        {
            var result = _context.MasterUsers.Where(x => x.CustID == userID).Count();
            if(result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CekEmailUser(string Email)
        {
            var result = _context.MasterUsers.Where(x => x.Email == Email).Count();
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //ubah password
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]

        public JsonResult ChangePassword(string CurrentPassword, string NewPassword, string ConfirmPassword)
        {
           
            //cek apakah cookie atau session yang digunakan untuk login
            var code = 500;
            string pesan = "";
            var regex = new Regex(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-_@#$%^&+=.,]).*$");
            
            if (!regex.IsMatch(NewPassword))
            {
                code = 500;
                pesan = "Your password must be at least 8 characters, a lowercase, a uppercase, a number and no space";
                return Json(new
                {
                    metaData = new { code = code, message = pesan },
                });
            }
            if (CurrentPassword == null || NewPassword == null || ConfirmPassword == null)
            {
                code = 500;
                pesan = "The data you entered is incomplete!";
                return Json(new
                {
                    metaData = new { code = code, message = pesan },
                });
            }
            if(NewPassword != ConfirmPassword)
            {
                code = 500;
                pesan = "New password not confirmed";
                return Json(new
                {
                    metaData = new { code = code, message = pesan },
                });
            }
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if(NewCookie != null)
            {
                var id = _global.Decrypt(NewCookie.Values.Get("CustID"));
                var user = GetByID(id);
                if(BCrypt.Net.BCrypt.Verify(CurrentPassword, user.Password))
                {
                    if((NewPassword == ConfirmPassword) && NewPassword != null)
                    {
                        user.Password = BCrypt.Net.BCrypt.HashPassword(NewPassword);
                        _context.SaveChanges();
                        code = 200;
                        pesan = "Password changed successfully!";
                    }
                    else
                    {
                        code = 500;
                        pesan = "New password doesn't match";
                    }
                }
                else
                {
                    code = 500;
                    pesan = "Your old password is wrong";
                }
            }
            if(Session["CustID"] != null)
            {
                var id = Session["CustID"];
                var user = _context.MasterUsers.Where(x => x.CustID == id).FirstOrDefault();
                if (BCrypt.Net.BCrypt.Verify(CurrentPassword, user.Password))
                {
                    if ((NewPassword == ConfirmPassword) && NewPassword != null)
                    {
                        user.Password = BCrypt.Net.BCrypt.HashPassword(NewPassword);
                        _context.SaveChanges();
                        code = 200;
                        pesan = "Password changed successfully!";
                    }
                    else
                    {
                        code = 500;
                        pesan = "New password doesn't match";
                    }
                }
                else
                {
                    code = 500;
                    pesan = "Your old password is wrong";
                }
            }
            return Json(new
            {
                metaData = new { code = code, message = pesan },
            });
        }
            
        public ActionResult GetListCustIDs()
        {
            var ListCustIds = GetEpicorDataRepo.Instance.GetListCustID().ToList();
            var data = ListCustIds.Select(x => x.CustID).ToList();
            ViewBag.custid = data;
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }
    }
}