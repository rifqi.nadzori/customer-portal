﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Helper;
using CustomerPortal.Models;



namespace CustomerPortal.Controllers
{
    public class HomeController : Controller
    {

        IGlobal _global = new GlobalHelper();
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();
        ERP10PresalesEntities _epicor = new ERP10PresalesEntities();
        public ActionResult Index()
        {
            
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            
            if (NewCookie == null && Session["role"] == null)
            {
                return RedirectToAction("Index","LoginPage");
            }
            if (NewCookie != null)
            {
                var role = _global.Decrypt(NewCookie.Values.Get("Role"));
                if(role == "admin")
                {
                    var Totalusers = _context.MasterUsers.Count();
                    var TotalUnVerified = _context.MasterUsers.Where(x => x.Verified == false).Count();
                    var TotalVerified = _context.MasterUsers.Where(x => x.Verified == true).Count();
                    var TotalOrders = 0;
                    ViewBag.users = Totalusers;
                    ViewBag.order = TotalOrders;
                    ViewBag.unverified = TotalUnVerified;
                    ViewBag.verified = TotalVerified;
                    ViewBag.role = role;
                    ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
                }
                else
                {
                    //data yang dibutuhkan untuk ditampilkan di halaman website di definisikan di sini
                    var id = _global.Decrypt(NewCookie.Values.Get("CustID"));
                    var TotalOrder = _context.OrderHeds.Where(x => x.CustID == id).Count();
                    var OrderToday = _context.OrderHeds.Where(x => x.OrderDate == DateTime.Today).Count();
                    ViewBag.totalorder = TotalOrder;
                    ViewBag.ordertoday = OrderToday;
                    ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
                    ViewBag.role = "customer";
                }
               
            }
            if(Session["role"] != null)
            {
                if (Session["role"] == "admin")
                {
                    var Totalusers = _context.MasterUsers.Count();
                    var TotalUnVerified = _context.MasterUsers.Where(x => x.Verified == false).Count();
                    var TotalVerified = _context.MasterUsers.Where(x => x.Verified == true).Count();
                    var TotalOrders = 0;
                    ViewBag.users = Totalusers;
                    ViewBag.order = TotalOrders;
                    ViewBag.unverified = TotalUnVerified;
                    ViewBag.verified = TotalVerified;
                    ViewBag.role = Session["role"];
                    ViewBag.value = Session["name"];
                }
                else
                {
                    //data yang dibutuhkan untuk ditampilkan di halaman website di definisikan di sini
                    var id = Session["CustID"];
                    var TotalOrder = _context.OrderHeds.Where(x => x.CustID == id).Count();
                    var OrderToday = _context.OrderHeds.Where(x => x.OrderDate == DateTime.Today).Count();
                    ViewBag.totalorder = TotalOrder;
                    ViewBag.ordertoday = OrderToday;
                    ViewBag.value = Session["name"];
                    ViewBag.role = "customer";
                }
            }
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult GetInactiveUser()
        {
            var InactiveUsers = _context.MasterUsers.Where(x => x.IsActive == false).ToList();
            return Json(new { data = InactiveUsers }, JsonRequestBehavior.AllowGet);
        }

        //----------Tambahan View Untuk Form----------------------//
        public ActionResult Table()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Register()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //----------End Tambahan View Untuk Form----------------------//
    }
}