﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Helper;

namespace CustomerPortal.Controllers
{
    public class HomeUserController : Controller
    {
        IGlobal _global = new GlobalHelper();
        // GET: HomeUser
        public ActionResult Index()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie == null)
            {
                return RedirectToAction("Login", "LoginPage");
            }
            //var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
            //ViewBag.value = user;
            ViewBag.cookies = NewCookie.Value;
            ViewBag.cookie = _global.Decrypt(NewCookie.Value);
            ViewBag.value = _global.Decrypt(NewCookie.Value);

            return View();
        }
    }
}