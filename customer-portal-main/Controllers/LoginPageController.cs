﻿using CustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CustomerPortal.Helper;
using BCrypt.Net;

namespace CustomerPortal.Controllers
{
    public class LoginPageController : Controller
    {
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();
        // GET: LoginPage
        IGlobal _global = new GlobalHelper();

        public ActionResult Index()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult verifySignIn()
        {
            dynamic metadata = "";
            try
            {
                metadata = new
                {
                    code = "200",
                    message = "Success Login."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Login"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Login()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie != null || Session["name"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Email, string Password, string Remember)
        {
            var pesan = "";
            var code = 404;
            var CekPassword = false;
            //string Company = System.Configuration.ConfigurationManager.ConnectionStrings["CustomerPortal.Properties.Settings.Company"].ConnectionString;
            string Company = Properties.Settings.Default.Company;

            //call API Epicor
            var IsAdminVerified = _global.VerifikasiAkunAdmin(Email, Password);
            
            if (IsAdminVerified)
            {
                if(Remember == "on")
                {
                    code = 200;
                    HttpCookie UserCookie = new HttpCookie("userCookie", _global.Encrypt(Email));
                    UserCookie.Values.Add("Role", _global.Encrypt("admin"));
                    UserCookie.Values.Add("CustName", _global.Encrypt(Email));
                    UserCookie.Expires.AddHours(24);
                    HttpContext.Response.SetCookie(UserCookie);
                    pesan = "Admin berhasil login";
                } else
                {
                    Session["name"] = Email;
                    Session["role"] = "admin";
                    pesan = "Admin berhasil login";
                    code = 200;
                }
                
                return Json(new
                {
                    metaData = new { code = code, message = pesan },
                    response = ""
                });
            }

            //jika yang login adalah customer
            var model = _context.MasterUsers.Where(x => x.Email == Email).FirstOrDefault();

            //cek jika user tidak ditemuakan atau belum terverifikasi
            if(model== null)
            {
                pesan = "Your email / password is wrong";
                code = 500;
            }
            if(model.Verified == false)
            {
                pesan = "Your Account is not verified";
                code = 500;
            }
            if(model != null && model.Verified == true)
            {
                CekPassword = BCrypt.Net.BCrypt.Verify(Password, model.Password);
                if (!CekPassword)
                {
                    pesan = "Wrong Password";
                    code = 500;
                    return Json(new
                    {
                        metaData = new { code = code, message = pesan },
                        response = ""
                    });
                }
                if(Remember == "on")
                {
                    HttpCookie UserCookie = new HttpCookie("userCookie");
                    UserCookie.Values.Add("Role", _global.Encrypt("customer"));
                    UserCookie.Values.Add("CustName", _global.Encrypt(model.Name));
                    UserCookie.Values.Add("CustID", _global.Encrypt(model.CustID));
                    UserCookie.Values.Add("Company", _global.Encrypt(Company));
                    UserCookie.Expires.AddHours(2);
                    HttpContext.Response.SetCookie(UserCookie);
                    Session["Company"] = Company;
                    Session["CustID"] = model.CustID;
                    code = 200;
                }
                else
                {
                    Session["email"] = Email;
                    Session["name"] = model.Name;
                    Session["CustID"] = model.CustID;
                    Session["role"] = "customer";
                    Session["Company"] = Company;
                    code = 200;
                }
                
            }
            return Json(new
            {
                metaData = new { code = code, message = pesan },
                response = ""
            });

        }       
        public ActionResult Logout()
        {
            HttpCookie deleteCookie = Request.Cookies["userCookie"];
            if(deleteCookie != null)
            {
                deleteCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(deleteCookie);
            }
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Login","LoginPage");
        }
    }
}