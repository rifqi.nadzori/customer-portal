﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Models;
using CustomerPortal.Helper;
using System.Data;
using System.Web.Optimization;
using System.Web.Routing;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using Ice.Core;
using System.Data.Entity;

namespace CustomerPortal.Controllers
{
    public class OrderAdminController : Controller
    {
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();
        IGlobal _global = new GlobalHelper();
        string CustomerID = string.Empty;
        // GET: OrderAdmin
        public ActionResult Index()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie == null && Session["role"] == null)
            {
                return RedirectToAction("Login", "LoginPage");
            }
            if (NewCookie != null)
            {
                var role = _global.Decrypt(NewCookie.Values.Get("Role"));
                if (role != "admin")
                {
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.role = _global.Decrypt(NewCookie.Values.Get("Role"));
                ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
            }
            if (Session["role"] != null)
            {
                var role = Session["role"];
                if (role != "admin")
                {
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.role = Session["role"];
                ViewBag.value = Session["name"];
            }
            return View();
        }

        [HttpPost]
        public ActionResult GetAllOrder()
        {
            var Order = (from OrderHed in _context.OrderHeds
                         join OrderDtl in _context.OrderDtls on OrderHed.OrderNum equals OrderDtl.OrderNum
                         select new
                         {
                             OrderHed.OrderNum,
                             OrderDtl.OrderLine,
                             OrderDtl.PartNum,
                             OrderDtl.Description,
                             OrderDtl.OrderQty,
                             OrderDtl.IUM,
                             OrderHed.NeedByDate,
                             OrderHed.Synced
                         }).ToList();
            return Json(new { data = Order }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult InsertBOSalesOrder(OrderHed order)
        //{
        //    var body = new OrderHed
        //    {
        //        OrderNum = order.OrderNum,
        //        CustID = order.CustID,
        //        CustName = order.CustName,
        //        PONum = order.PONum,
        //        OrderDate = order.OrderDate,
        //        NeedByDate = order.NeedByDate,
        //    };

        //    _context.OrderHeds.Add(body);
        //    _context.SaveChanges();
        //    return Json(new
        //    {
        //        metaData = new { code = 200, message = "New Order Head Created Successfully" },
        //        //response = order
        //        response = body.OrderNum
        //    });
        //}

        [HttpPost, ActionName("SyncEpicor")]
        
        public ActionResult SyncToEpicor(string Username, string Password)
        {
            dynamic metadata = "";
            using (_context)
            {
                string val = "";

                try
                {
                    val = SalesOrderRepository.Instance.InsertSalesOrder(Username, Password);
                    if (val != "Success")
                    {
                        metadata = new
                        {
                            code = "400",
                            message = val
                        };

                    }
                    else
                    {
                        /*orderHeds.Synced = true;
                        _context.OrderHeds.Add(orderHeds);
                        _context.Entry(orderHeds).State = EntityState.Modified;
                        _context.SaveChanges();*/
                        metadata = new
                        {
                            code = 200,
                            message = val
                        };

                    }
                    return Json(new { metaData = metadata, response = 1 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    val = e.Message;
                    return Json(new { metaData = val, response = 1 }, JsonRequestBehavior.AllowGet); ;
                }
            }
        }
    }
}