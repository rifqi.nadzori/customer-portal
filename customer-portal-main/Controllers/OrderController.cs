﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Models;
using CustomerPortal.Helper;
using System.Data;
using System.Web.Optimization;
using System.Web.Routing;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;

namespace CustomerPortal.Controllers
{
    public class OrderController : Controller
    {
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();
        IGlobal _global = new GlobalHelper();
        string CustomerID = string.Empty;
        // GET: Order
        public ActionResult Index()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie == null && Session["role"] == null)
            {
                return RedirectToAction("Login", "LoginPage");
            }
            if (NewCookie != null)
            {
                ViewBag.role = _global.Decrypt(NewCookie.Values.Get("Role"));
                ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
                ViewBag.CustID = _global.Decrypt(NewCookie.Values.Get("CustID"));
                ViewBag.CustName = _global.Decrypt(NewCookie.Values.Get("CustName"));
            }
            if (Session["role"] != null)
            {
                ViewBag.role = Session["role"];
                ViewBag.value = Session["name"];
                ViewBag.CustID = Session["CustID"];
                ViewBag.CustName = Session["name"];
            }
            return View();
        }
        public ActionResult List()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie == null && Session["role"] == null)
            {
                return RedirectToAction("Login", "LoginPage");
            }
            if (NewCookie != null)
            {
                ViewBag.role = _global.Decrypt(NewCookie.Values.Get("Role"));
                ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
            }
            if (Session["role"] != null)
            {
                ViewBag.role = Session["role"];
                ViewBag.value = Session["name"];
            }
            return View();
        }

        [HttpPost]
        public ActionResult GetAllSalesOrder()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie != null)
            {
                ViewBag.role = _global.Decrypt(NewCookie.Values.Get("Role"));
                ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
                ViewBag.CustID = _global.Decrypt(NewCookie.Values.Get("CustID"));
                CustomerID = Convert.ToString(_global.Decrypt(NewCookie.Values.Get("CustID")));
                ViewBag.CustName = _global.Decrypt(NewCookie.Values.Get("CustName"));
            }
            if (Session["role"] != null)
            {
                ViewBag.role = Session["role"];
                ViewBag.value = Session["name"];
                ViewBag.CustID = Session["CustID"];
                CustomerID = Convert.ToString(Session["CustID"]);
                ViewBag.CustName = Session["name"];
            }
            var Order = (from OrderHed in _context.OrderHeds
                         where OrderHed.CustID == CustomerID
                         //join OrderDtl in _context.OrderDtls on OrderHed.OrderNum equals OrderDtl.OrderNum
                         select new
                         {
                             OrderHed.OrderNum,
                             OrderHed.OrderDate,
                             OrderHed.PONum,
                             OrderHed.CustID,
                             OrderHed.CustName
                         }).ToList();
            return Json(new { data = Order }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult GetAllOrder(int OrderNum)
        public JsonResult GetAllOrder(int OrderNum)
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie != null)
            {
                ViewBag.role = _global.Decrypt(NewCookie.Values.Get("Role"));
                ViewBag.value = _global.Decrypt(NewCookie.Values.Get("CustName"));
                ViewBag.CustID = _global.Decrypt(NewCookie.Values.Get("CustID"));
                CustomerID = Convert.ToString(_global.Decrypt(NewCookie.Values.Get("CustID")));
                ViewBag.CustName = _global.Decrypt(NewCookie.Values.Get("CustName"));
            }
            if (Session["role"] != null)
            {
                ViewBag.role = Session["role"];
                ViewBag.value = Session["name"];
                ViewBag.CustID = Session["CustID"];
                CustomerID = Convert.ToString(Session["CustID"]);
                ViewBag.CustName = Session["name"];
            }

            var Order = (from OrderHed in _context.OrderHeds
                         join OrderDtl in _context.OrderDtls on OrderHed.OrderNum equals OrderDtl.OrderNum
                         where OrderHed.CustID == CustomerID && OrderHed.OrderNum == OrderNum
                         select new
                         {
                             OrderHed.OrderNum,
                             OrderDtl.OrderLine,
                             OrderDtl.PartNum,
                             OrderDtl.Description,
                             OrderDtl.OrderQty,
                             OrderDtl.IUM,
                             OrderHed.NeedByDate
                         }).ToList();
            //var data = Order.ToList();
            //return Json(new
            //{
            //    metaData = new { code = 200, message = "All Users Loaded Successfully" },
            //    response = data
            //});
            return Json(new { data = Order }, JsonRequestBehavior.AllowGet);
        }

        //POST: Add New OrderHed
        //[HttpPost]
        //public ActionResult SaveOrderHed(OrderHed order)
        //{
        //    var body = new OrderHed
        //    {
        //        OrderNum = Convert.ToInt32(GetOrderNum()),
        //        CustID = order.CustID,
        //        CustName = order.CustName,
        //        PONum = order.PONum,
        //        OrderDate = order.OrderDate,
        //        NeedByDate = order.NeedByDate,
        //    };
            
        //    _context.OrderHeds.Add(body);
        //    _context.SaveChanges();
        //    return Json(new
        //    {
        //        metaData = new { code = 200, message = "New Order Head Created Successfully" },
        //        response = body.OrderNum
        //    });
        //}

        [HttpPost]
        public ActionResult SaveOrderHed(OrderHed order)
        {
            string alert = string.Empty;
            Int64 OrderNum;

            if(order.OrderNum == 0)
            {
                var body = new OrderHed
                {
                    OrderNum = Convert.ToInt32(GetOrderNum()),
                    CustID = order.CustID,
                    CustName = order.CustName,
                    PONum = order.PONum,
                    OrderDate = order.OrderDate,
                    NeedByDate = order.NeedByDate,
                };
                _context.OrderHeds.Add(body);
                _context.SaveChanges();

                OrderNum = body.OrderNum;
                //alert = "ini nambah";
            }
            else
            {
                var body = new OrderHed
                {
                    OrderNum = order.OrderNum,
                    CustID = order.CustID,
                    CustName = order.CustName,
                    PONum = order.PONum,
                    OrderDate = order.OrderDate,
                    NeedByDate = order.NeedByDate,
                };

                //_context.SaveChanges();
                OrderHedRepository.Instance.UpdateOrderHed(body);
                OrderNum = body.OrderNum;
                //alert = "ini update";
            }


            //_context.OrderHeds.Add(body);
           // _context.SaveChanges();
            return Json(new
            {
                metaData = new { code = 200, message = "New Order Created / Updated Successfully" },
                //response = body.OrderNum
                response = OrderNum
                //response = alert
            });
        }

        //POST: Add New OrderDetail
        [HttpPost]
        public ActionResult SaveOrderDetail(OrderDtl orderdtl)
        {
            var bodydtl = new OrderDtl
            {
                OrderNum = orderdtl.OrderNum,
                OrderLine = Convert.ToInt32(GetOrderLine(orderdtl.OrderNum)),
                //OrderLine = 1,
                PartNum = orderdtl.PartNum,
                Description = orderdtl.Description,
                IUM = orderdtl.IUM,
                OrderQty = orderdtl.OrderQty,
                UnitPrice = orderdtl.UnitPrice,
            };
            _context.OrderDtls.Add(bodydtl);
            _context.SaveChanges();
            return Json(new
            {
                metaData = new { code = 200, message = "New Order Detail Created Successfully" },
                response = bodydtl.OrderLine
            });
        }

        public ActionResult Show(int OrderNum)
        {
            var _DBCustomerPortalEntities = _context.OrderHeds.Find(OrderNum);
            var OrderHed = new OrderHed
            { 
                OrderNum = _DBCustomerPortalEntities.OrderNum,
                CustID = _DBCustomerPortalEntities.CustID,
                CustName = _DBCustomerPortalEntities.CustName,
                PONum = _DBCustomerPortalEntities.PONum,
                OrderDate = _DBCustomerPortalEntities.OrderDate,
                NeedByDate = _DBCustomerPortalEntities.NeedByDate,

            };
            dynamic metadata = "";
            if (OrderNum.ToString() == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "OrderNum is required."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            if (_DBCustomerPortalEntities == null)
            {
                metadata = new
                {
                    code = "404",
                    message = "Order Not Found."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            metadata = new
            {
                code = "200",
                message = "OK"
            };
            return Json(new { metaData = metadata, response = OrderHed }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowOrderDetail(int OrderNum)
        {
            int minOrderLine = 1;
            var OrderLineList = (from OrderDetail in _context.OrderDtls
                         where OrderDetail.OrderNum == OrderNum
                         select new
                         {
                             OrderDetail.OrderLine,
                         }).ToList();
            if(OrderLineList.Count != 0)
            {
                minOrderLine = OrderLineList.Min(x => x.OrderLine);
            }
            var _DBCustomerPortalEntities = _context.OrderDtls.Find(OrderNum, minOrderLine);
            dynamic metadata = "";
            if (OrderNum.ToString() == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "OrderNum is required."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            if (_DBCustomerPortalEntities == null)
            {
                metadata = new
                {
                    code = "404",
                    message = "Order Not Found."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            var OrderDtl = new OrderDtl
            {
                OrderNum = _DBCustomerPortalEntities.OrderNum,
                OrderLine = _DBCustomerPortalEntities.OrderLine,
                PartNum = _DBCustomerPortalEntities.PartNum,
                Description = _DBCustomerPortalEntities.Description,
                IUM = _DBCustomerPortalEntities.IUM,
                OrderQty = _DBCustomerPortalEntities.OrderQty,
                UnitPrice = _DBCustomerPortalEntities.UnitPrice,
            };

            metadata = new
            {
                code = "200",
                message = "OK"
            };
            return Json(new { metaData = metadata, response = OrderDtl }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPartNum()
        {
            List<PartNumber> list = PartNumRepository.Instance.GetListPartNum();
            List<PartNumber> datas = new List<PartNumber>();
            foreach (var item in list)
            {
                datas.Add(new PartNumber()
                {
                    PartNum = item.PartNum,
                    PartDescription = item.PartDescription,
                    ClassID = item.ClassID,
                    IUM = item.IUM,
                    TypeCode = item.TypeCode,
                    ProdCode = item.ProdCode,
                    UnitPrice = item.UnitPrice,
                });
            }
            return Json(new { data = datas }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult DeleteOrderHed (int numberOrder)
        //{
        //    var order = GetByOrderNumber(numberOrder);
        //    _context.OrderHeds.Remove(order);
        //    _context.SaveChanges();
        //    return Json(new
        //    {
        //        metaData = new { code = 200, message = "Order Hed Removed Successfully" },
        //        response = ""
        //    });
        //}

        [HttpPost, ActionName("DeleteOrderHed")]
        public ActionResult DeleteOrderHed(int OrderNum)
        {
            List<OrderHed> valResult = new List<OrderHed>();
            valResult = OrderHedRepository.Instance.DeleteOrderHed(OrderNum);
            //return Json(valResult, JsonRequestBehavior.AllowGet);
            return Json(new
            {
                metaData = new { code = 200, message = "Order Deleted Successfully" },
            });
        }

        [HttpPost, ActionName("DeleteOrderDetail")]
        public ActionResult DeleteOrderDetail(int OrderNum, int OrderLine)
        {
            List<OrderDtl> valResult = new List<OrderDtl>();
            valResult = OrderHedRepository.Instance.DeleteOrderDetail(OrderNum, OrderLine);

            int minOrderLine = 1;
            var OrderLineList = (from OrderDetail in _context.OrderDtls
                                 where OrderDetail.OrderNum == OrderNum
                                 select new
                                 {
                                     OrderDetail.OrderLine,
                                 }).ToList();
            if (OrderLineList.Count != 0)
            {
                minOrderLine = OrderLineList.Min(x => x.OrderLine);
            }
            //return Json(valResult, JsonRequestBehavior.AllowGet);
            return Json(new
            {
                metaData = new { code = 200, message = "Order Detail Deleted Successfully" },
                response = minOrderLine
            });
        }

        [HttpPost]
        public JsonResult AmbilOrder(int numberorder)
        {
            //string custId = Request.Form.GetValues("custId").FirstOrDefault();
            var order = GetByOrderNumber(numberorder);
            return Json(new
            {
                metaData = new { code = 200, message = "OK" },
                response = order
            });
        }

        [HttpPost]
        public ActionResult Update(int OrderNum, OrderHed order)
        {
            var selectedOrder = GetByOrderNumber(OrderNum);
            selectedOrder.OrderNum = order.OrderNum;
            selectedOrder.CustID = order.CustID;
            selectedOrder.CustName = order.CustName;
            selectedOrder.OrderDate = order.OrderDate;
            selectedOrder.NeedByDate = order.NeedByDate;
            selectedOrder.PONum = order.PONum;
            _context.SaveChanges();
            return Json(new
            {
                metaData = new { code = 200, message = "Order Updated Successfully" },
                response = order
            });
        }

        //fungsi untuk mengambil data order berdasarkan ID
        public OrderHed GetByOrderNumber(int numberOrder)
        {
            return _context.OrderHeds.Where(x => x.OrderNum == numberOrder).FirstOrDefault();
        }

        public String GetOrderNum()
        {
            try
            {
                string GenerateOrderNum = "";
                using (DBCustomerPortalEntities db = new DBCustomerPortalEntities())
                {
                    string lastOrder = Convert.ToString(db.OrderHeds.OrderByDescending(e => e.OrderNum).FirstOrDefault()?.OrderNum);
                    if (lastOrder == "")
                    {
                        GenerateOrderNum = "1";
                        return GenerateOrderNum;
                    }
                    else
                    {
                        int lastOrderNum = int.Parse(lastOrder);
                        int newOrderNum = lastOrderNum + 1;
                        GenerateOrderNum = Convert.ToString(newOrderNum);
                        return GenerateOrderNum;
                    } 
                }
            }
            catch (Exception e)
            {
                return ("Not Success :" + e.Message);
            }
        }
        public String GetOrderLine(int OrderNum)
        {
            string GenerateOrderLine = "";
            try
            {
                using (DBCustomerPortalEntities db = new DBCustomerPortalEntities())
                {
                    //var lastOrderLine = Convert.ToString(db.OrderDtls.Where(e => e.OrderNum == Convert.ToInt32(GetOrderNum())).OrderByDescending(e => e.OrderLine).FirstOrDefault()?.OrderLine);
                    var lastOrderLine = db.OrderDtls.Where(e => e.OrderNum == OrderNum).OrderByDescending(e => e.OrderLine).FirstOrDefault()?.OrderLine;

                    if (lastOrderLine == null)
                    {
                        GenerateOrderLine = "1";
                        return GenerateOrderLine;
                    }
                    else
                    {
                        int lastOrderDetailLine = int.Parse(Convert.ToString(lastOrderLine));
                        int newOrderLine = lastOrderDetailLine + 1;
                        GenerateOrderLine = Convert.ToString(newOrderLine);
                        return GenerateOrderLine;
                    }
                }
            }
            catch (Exception e)
            {
                //return ("Not Success :" + e.Message);
                GenerateOrderLine = "1";
                return GenerateOrderLine;
            }
        }

        public JsonResult GetInactiveUser()
        {
            var InactiveUsers = _context.MasterUsers.Where(x => x.IsActive == false).ToList();
            return Json(new { data = InactiveUsers }, JsonRequestBehavior.AllowGet);
        }
    }
}