﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CustomerPortal.Helper;
using Newtonsoft.Json.Linq;
using CustomerPortal.Models;
using Newtonsoft.Json;
using CustomerPortal.Repository;
using System.Data;

namespace CustomerPortal.Controllers
{
    public class ReportController : Controller
    {
        IGlobal _global = new GlobalHelper();
        //ERP10PresalesEntities _epicor = new ERP10PresalesEntities();

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ReportAdmin()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie == null && Session["role"] == null)
            {
                return RedirectToAction("Index", "LoginPage");
            }
            if (NewCookie == null && Session["role"] != null)
            {
                ViewBag.role = "admin";
                return View();
            }
            var admin = NewCookie.Values.Get("Role");
            if (admin != _global.Encrypt("admin") && Session["role"] != "admin")
            {
                return RedirectToAction("Index", "LoginPage");
            }
            ViewBag.role = "admin";
            return View();
        }

        public ActionResult ReportInvoice()
        {
            HttpCookie NewCookie = Request.Cookies["userCookie"];
            if (NewCookie == null && Session["role"] == null)
            {
                return RedirectToAction("Index", "LoginPage");
            }
            ViewBag.role = "customer";
            return View();
        }

        [HttpPost]
        public ActionResult GetInvoiceData()
        {
            //var epicor = _epicor.InvcHeads.Where(x => x.InvoiceNum < 8050).ToList();
            var DateFrom = Request.Form.GetValues("DateFrom").FirstOrDefault();
            var DateTo = Request.Form.GetValues("DateTo").FirstOrDefault();
            var search = Request.Form.GetValues("search[value]").FirstOrDefault().ToLower();
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            string sortColumn = Request.Form.GetValues("order[0][column]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;

            var CompanySession = Session["Company"].ToString();
            var CustIDSession = Session["CustID"].ToString();
            HttpCookie UserCookie = Request.Cookies["userCookie"];
            if (UserCookie != null)
            {
                CompanySession = _global.Decrypt(UserCookie.Values.Get("Company"));
                CustIDSession = _global.Decrypt(UserCookie.Values.Get("CustID"));
            }
            var invoices = GetEpicorDataRepo.Instance.GetAllDatas(CustIDSession, CompanySession).ToList();
            totalRecords = invoices.Count();

            //searcing

            if (DateFrom != "" && DateTo != "")
                {
                    DateTime StartDate = DateTime.Parse(DateFrom);
                    DateTime EndDate = DateTime.Parse(DateTo);
                    invoices = invoices.Where(x => x.InvoiceDate >= StartDate && x.InvoiceDate <= EndDate).ToList();
                    totalRecords = invoices.Count();

                    //jika form search diisi saat form filter juga terisi
                    if (!string.IsNullOrEmpty(search))
                    {
                        invoices = invoices.Where(x => x.CustID.ToLower().Contains(search)
                        || x.InvoiceNum.ToString().Contains(search)
                        || x.Name.ToLower().Contains(search)).ToList();
                        totalRecords = invoices.Count();
                        if (!(string.IsNullOrEmpty(sortColumn.ToString()) && string.IsNullOrEmpty(sortColumnDir)))
                        {
                            if (sortColumnDir == "asc")
                            {
                                if (sortColumn == "0")
                                {
                                    invoices = invoices.OrderBy(x => x.InvoiceNum).ToList();
                                    totalRecords = invoices.Count();
                                }
                                else if (sortColumn == "1")
                                {
                                    invoices = invoices.OrderBy(x => x.CustID).ToList();
                                    totalRecords = invoices.Count();
                                    
                                }
                                else if (sortColumn == "2")
                                {
                                    invoices = invoices.OrderBy(x => x.Name).ToList();
                                    totalRecords = invoices.Count();
                                    
                                }
                                else if (sortColumn == "3")
                                {
                                invoices = invoices.OrderBy(x => x.PONum).ToList();
                                totalRecords = invoices.Count();
                            }
                                else if (sortColumn == "4")
                                {
                                invoices = invoices.OrderBy(x => x.OrderNum).ToList();
                                totalRecords = invoices.Count();
                            }
                                else
                                {
                                invoices = invoices.OrderBy(x => x.InvoiceDate).ToList();
                                totalRecords = invoices.Count();
                            }
                            }
                            else
                            {
                                if (sortColumn == "0")
                                {
                                    invoices = invoices.OrderByDescending(x => x.InvoiceNum).ToList();
                                    totalRecords = invoices.Count();
                                }
                                else if (sortColumn == "1")
                                {
                                invoices = invoices.OrderByDescending(x => x.CustID).ToList();
                                totalRecords = invoices.Count();
                            }
                                else if (sortColumn == "2")
                                {
                                invoices = invoices.OrderByDescending(x => x.Name).ToList();
                                totalRecords = invoices.Count();
                            }
                                else if (sortColumn == "3")
                                {
                                invoices = invoices.OrderByDescending(x => x.PONum).ToList();
                                totalRecords = invoices.Count();
                            }
                                else if (sortColumn == "4")
                                {
                                invoices = invoices.OrderByDescending(x => x.OrderNum).ToList();
                                totalRecords = invoices.Count();
                            }
                            else
                                {
                                invoices = invoices.OrderByDescending(x => x.InvoiceDate).ToList();
                                totalRecords = invoices.Count();
                            }
                            }
                        }
                    }
                    //jika header sort ditekan saat form filter tanggal kosong
                    if (!((string.IsNullOrEmpty(sortColumn.ToString()) && string.IsNullOrEmpty(sortColumnDir)) && string.IsNullOrEmpty(search)))
                    {
                    if (sortColumnDir == "asc")
                    {
                        if (sortColumn == "0")
                        {
                            invoices = invoices.OrderBy(x => x.InvoiceNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "1")
                        {
                            invoices = invoices.OrderBy(x => x.CustID).ToList();
                            totalRecords = invoices.Count();

                        }
                        else if (sortColumn == "2")
                        {
                            invoices = invoices.OrderBy(x => x.Name).ToList();
                            totalRecords = invoices.Count();

                        }
                        else if (sortColumn == "3")
                        {
                            invoices = invoices.OrderBy(x => x.PONum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "4")
                        {
                            invoices = invoices.OrderBy(x => x.OrderNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else
                        {
                            invoices = invoices.OrderBy(x => x.InvoiceDate).ToList();
                            totalRecords = invoices.Count();
                        }
                    }
                    else
                    {
                        if (sortColumn == "0")
                        {
                            invoices = invoices.OrderByDescending(x => x.InvoiceNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "1")
                        {
                            invoices = invoices.OrderByDescending(x => x.CustID).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "2")
                        {
                            invoices = invoices.OrderByDescending(x => x.Name).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "3")
                        {
                            invoices = invoices.OrderByDescending(x => x.PONum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "4")
                        {
                            invoices = invoices.OrderByDescending(x => x.OrderNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else
                        {
                            invoices = invoices.OrderByDescending(x => x.InvoiceDate).ToList();
                            totalRecords = invoices.Count();
                        }
                    }
                }
                }
                else
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        invoices = invoices.Where(x => x.CustID.ToLower().Contains(search)
                        || x.InvoiceNum.ToString().Contains(search)
                        || x.Name.ToLower().Contains(search)).ToList();
                        totalRecords = invoices.Count();
                        
                        if (!(string.IsNullOrEmpty(sortColumn.ToString()) && string.IsNullOrEmpty(sortColumnDir)))
                        {
                        if (sortColumnDir == "asc")
                        {
                            if (sortColumn == "0")
                            {
                                invoices = invoices.OrderBy(x => x.InvoiceNum).ToList();
                                totalRecords = invoices.Count();
                            }
                            else if (sortColumn == "1")
                            {
                                invoices = invoices.OrderBy(x => x.CustID).ToList();
                                totalRecords = invoices.Count();

                            }
                            else if (sortColumn == "2")
                            {
                                invoices = invoices.OrderBy(x => x.Name).ToList();
                                totalRecords = invoices.Count();

                            }
                            else if (sortColumn == "3")
                            {
                                invoices = invoices.OrderBy(x => x.PONum).ToList();
                                totalRecords = invoices.Count();
                            }
                            else if (sortColumn == "4")
                            {
                                invoices = invoices.OrderBy(x => x.OrderNum).ToList();
                                totalRecords = invoices.Count();
                            }
                            else
                            {
                                invoices = invoices.OrderBy(x => x.InvoiceDate).ToList();
                                totalRecords = invoices.Count();
                            }
                        }
                        else
                        {
                            if (sortColumn == "0")
                            {
                                invoices = invoices.OrderByDescending(x => x.InvoiceNum).ToList();
                                totalRecords = invoices.Count();
                            }
                            else if (sortColumn == "1")
                            {
                                invoices = invoices.OrderByDescending(x => x.CustID).ToList();
                                totalRecords = invoices.Count();
                            }
                            else if (sortColumn == "2")
                            {
                                invoices = invoices.OrderByDescending(x => x.Name).ToList();
                                totalRecords = invoices.Count();
                            }
                            else if (sortColumn == "3")
                            {
                                invoices = invoices.OrderByDescending(x => x.PONum).ToList();
                                totalRecords = invoices.Count();
                            }
                            else if (sortColumn == "4")
                            {
                                invoices = invoices.OrderByDescending(x => x.OrderNum).ToList();
                                totalRecords = invoices.Count();
                            }
                            else
                            {
                                invoices = invoices.OrderByDescending(x => x.InvoiceDate).ToList();
                                totalRecords = invoices.Count();
                            }
                        }
                    }
                 }
                    //jika header sort ditekan dan form filter tanggal kosong
                    if ((!(string.IsNullOrEmpty(sortColumn.ToString()) && string.IsNullOrEmpty(sortColumnDir)) && string.IsNullOrEmpty(search)))
                    {
                    if (sortColumnDir == "asc")
                    {
                        if (sortColumn == "0")
                        {
                            invoices = invoices.OrderBy(x => x.InvoiceNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "1")
                        {
                            invoices = invoices.OrderBy(x => x.CustID).ToList();
                            totalRecords = invoices.Count();

                        }
                        else if (sortColumn == "2")
                        {
                            invoices = invoices.OrderBy(x => x.Name).ToList();
                            totalRecords = invoices.Count();

                        }
                        else if (sortColumn == "3")
                        {
                            invoices = invoices.OrderBy(x => x.PONum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "4")
                        {
                            invoices = invoices.OrderBy(x => x.OrderNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else
                        {
                            invoices = invoices.OrderBy(x => x.InvoiceDate).ToList();
                            totalRecords = invoices.Count();
                        }
                    }
                    else
                    {
                        if (sortColumn == "0")
                        {
                            invoices = invoices.OrderByDescending(x => x.InvoiceNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "1")
                        {
                            invoices = invoices.OrderByDescending(x => x.CustID).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "2")
                        {
                            invoices = invoices.OrderByDescending(x => x.Name).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "3")
                        {
                            invoices = invoices.OrderByDescending(x => x.PONum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else if (sortColumn == "4")
                        {
                            invoices = invoices.OrderByDescending(x => x.OrderNum).ToList();
                            totalRecords = invoices.Count();
                        }
                        else
                        {
                            invoices = invoices.OrderByDescending(x => x.InvoiceDate).ToList();
                            totalRecords = invoices.Count();
                        }
                    }
                }
                }
            var DataTampil = invoices.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = DataTampil }, JsonRequestBehavior.AllowGet);

        }
    }
}