﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CustomerPortal
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DBCustomerPortalEntities : DbContext
    {
        public DBCustomerPortalEntities()
            : base("name=DBCustomerPortalEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<OrderDtl> OrderDtls { get; set; }
        public virtual DbSet<OrderHed> OrderHeds { get; set; }
        public virtual DbSet<MasterUser> MasterUsers { get; set; }
    }
}
