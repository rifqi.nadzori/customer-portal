﻿using CustomerPortal.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CustomerPortal.Helper
{

    public interface IGlobal
    {
        string Encrypt(string clearText);
        string Decrypt(string cipherText);
        bool VerifikasiAkunAdmin(string username, string password);
        string GeneratePassword();
        void KirimkanEmail(string Subject, string BodyEmail, string EmailTujuan);
        string GenerateBodyEmail(string status, string email, string password, string custId);
        string GetInvoiceData();
        List<InvoiceModel> GetApiData(string url);
    }
    public class GlobalHelper : IGlobal
    {
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();
        static readonly char[] padding = { '=' };
        public string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "JHK@!$RTYEWQRGH";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray()).TrimEnd(padding).Replace('+', '-').Replace('/', '_');
                    }
                }
                return clearText;
            }
            catch
            {

            }

            return "";
        }

        public string Decrypt(string cipherText)
        {
            string incoming = cipherText.Replace('_', '/').Replace('-', '+');
            switch (cipherText.Length % 4)
            {
                case 2: incoming += "=="; break;
                case 3: incoming += "="; break;
            }
            try
            {
                string EncryptionKey = "JHK@!$RTYEWQRGH";
                byte[] cipherBytes = Convert.FromBase64String(incoming);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return cipherText;
            }
            catch
            {

            }
            return "";
        }

        public bool VerifikasiAkunAdmin(string username, string password)
        {
            using (var client = new HttpClient())
            {
                string auth = username + ":" + password;
                byte[] inArray = UTF8Encoding.UTF8.GetBytes(auth);
                client.BaseAddress = new Uri($"https://erp.intalogi.com/ERP10Presales/api/v1/Erp.BO.UserFileSvc/UserFiles()");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(inArray));
                var responseTask = client.GetAsync($"https://erp.intalogi.com/ERP10Presales/api/v1/Erp.BO.UserFileSvc/UserFiles()");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
            }
            return false;
        }

        public string GeneratePassword()
        {
            Random rand = new Random();
            int stringlen = rand.Next(8, 10);
            int randValue;
            string str = "";
            char letter;
            for (int i = 0; i < stringlen; i++)
            {
                randValue = rand.Next(0, 26);
                letter = Convert.ToChar(randValue + 65);
                str = str + letter;
            }
            return str;
        }

        public void KirimkanEmail(string Subject, string BodyEmail, string EmailTujuan)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("anthonstr@gmail.com");
                mail.To.Add(EmailTujuan);
                mail.Subject = Subject;
                mail.Body = BodyEmail;
                mail.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    smtp.Credentials = new NetworkCredential("anthonstr@gmail.com", "mexpppqxumbvfmyx");
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                }
            }
        }

        public string GenerateBodyEmail(string status, string email, string password, string custId)
        {
            var viewPath = "~/Views/Email/Template.cshtml";
            var EmailBody = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(viewPath));
            string content = "";

            if (status == "false")
            {
                var url = "https://localhost:44334/Account/Verified/" + Encrypt(custId);
                content = "<span style='font-size: 16px;'>Halo, <b>" + email + "</b></span>";
                content += "<p style='font-size: 12px;'>Email anda (" + email + ") telah terdaftar sebagai akun User Customer Portal. Untuk menyelesaikan proses registrasi akun, Silakan gunakan email dan password berikut untuk melakukan proses login:</p>";
                content += "<p>Email : " + email + " </p>";
                content += "<p>Password : " + password + " </p>";
                content += "<center><a href='" + url + "' target='_blank' class='btn-primary' style='box-sizing: border-box; font-size: 13px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #6658dd; margin: 0; border-color: #6658dd; border-style: solid; border-width: 8px 16px;'>Vefirikasi Akun</a></center>";
                content += "<hr style='margin-top: 1.2rem;margin-bottom: 1.5rem;border: 0;border-top: 1px solid #e5e8eb;'>";

                content += "<p style='font-size: 11px;color: #999;'>Jika anda mengalami kesulitan dalam melakukan klik tombol <b>Aktivasi</b>, silakan hubungi admin</p>";
                EmailBody = EmailBody.Replace("{{Content-Body}}", content);
            }
            else
            {
                content = "<span style='font-size: 16px;'>Halo, <b>" + email + "</b></span>";
                content += "<p style='font-size: 12px;'>Email anda (" + email + ") telah kami nonaktifkan dari sistem kami </p>";
                content += "<p style='font-size: 12px;'>Terima Kasih Telah bekerja sama dengan perusahaan kami </p>";
                content += "<hr style='margin-top: 1.2rem;margin-bottom: 1.5rem;border: 0;border-top: 1px solid #e5e8eb;'>";

                EmailBody = EmailBody.Replace("{{Content-Body}}", content);
            }
            return EmailBody;
        }

        public string GetInvoiceData()
        {
            using (var client = new HttpClient())
            {
                string auth = "manager" + ":" + "manager";
                byte[] inArray = UTF8Encoding.UTF8.GetBytes(auth);
                client.BaseAddress = new Uri($"https://erp.intalogi.com/ERP10Presales/api/v1/BaqSvc/UDAH_GetDAtaInvoice/");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(inArray));
                var responseTask = client.GetAsync($"https://erp.intalogi.com/ERP10Presales/api/v1/BaqSvc/UDAH_GetDAtaInvoice/");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    var data = readTask.Result;
                }
                else
                {
                    Console.WriteLine(result.StatusCode);
                }
                return "data";
            }
        }

        public List<InvoiceModel> GetApiData(string url)
        {
            dynamic datas;
            using (var client = new HttpClient())
            {
                string auth = "manager" + ":" + "manager";
                //GET data from API
                byte[] inArray = UTF8Encoding.UTF8.GetBytes(auth);
                client.BaseAddress = new Uri($"{url}");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(inArray));
                var responseTask = client.GetAsync($"{url}");
                responseTask.Wait();
                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    var RawData = readTask.Result;
                    JObject json = JObject.Parse(RawData);
                    var DataString = json["value"].ToString();
                    List<InvoiceModel> ListData = new List<InvoiceModel>();
                    ListData = JsonConvert.DeserializeObject<List<InvoiceModel>>(DataString);
                    return ListData;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
       