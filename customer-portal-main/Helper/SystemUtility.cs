﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace CustomerPortal.Helper
{
    public class SystemUtility : SQLConnect
    {
        public string getSystemValue(string systemId)
        {
            string sql = "SELECT SystemValue FROM TB_M_SYSTEM WHERE SystemId='" + systemId + "'";
            DataTable dt = GetDataTable(sql);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["SystemValue"].ToString();
            }
            else
            {
                return "";
            }
        }
    }
}