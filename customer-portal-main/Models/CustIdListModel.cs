﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class CustIdListModel
    {
        public string CustID { get; set; }
        public string Name { get; set; }
    }
}