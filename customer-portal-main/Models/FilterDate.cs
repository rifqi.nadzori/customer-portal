﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class FilterDate
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}