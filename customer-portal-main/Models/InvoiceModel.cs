﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class InvoiceModel
    {
        public int InvcHead_InvoiceNum { get; set; }
        public int InvcHead_CustNum { get; set; }
        public string InvcHead_PONum { get; set; }
        public string InvcHead_InvoiceAmt { get; set; }
        public string InvcHead_DocInvoiceAmt { get; set; }
        public int InvcHead_OrderNum { get; set; }
        public string Customer_CustID { get; set; }
        public string Customer_Name { get; set; }
        public DateTime InvcHead_InvoiceDate { get; set; }
        public string RowIdent { get; set; }
    }
}
