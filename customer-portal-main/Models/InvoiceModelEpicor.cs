﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class InvoiceModelEpicor
    {
        public int InvoiceNum { get; set; }
        public int CustNum { get; set; }
        public string PONum { get; set; }
        public decimal InvoiceAmt { get; set; }
        public decimal DocInvoiceAmt { get; set; }
        public int OrderNum { get; set; }
        public string CustID { get; set; }
        public string Name { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string RowIdent { get; set; }
    }
}