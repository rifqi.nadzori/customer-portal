﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using CustomerPortal.Helper;
using CustomerPortal.Models;
using System.Data;
using System.Web.Mvc;


namespace CustomerPortal.Models
{
    public class OrderHedRepository : SQLConnectCustomer
    {
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();
        private static OrderHedRepository instance = null;
        public static OrderHedRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OrderHedRepository();
                }

                return instance;
            }
        }
        public List<dynamic> getDataOrderHed()
        {

            List<dynamic> listval = new List<dynamic>();
            List<OrderHed> listKaryawan = new List<OrderHed>();
            List<OrderHed> listOrArray = new List<OrderHed>();
            //List<OrderHedArray> listKrArray = new List<MasterKaryawanArray>();
            try
            {
                //using (var client = new HttpClient())
                {
                    //client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //string _url = "api/Karyawan/GetKaryawanJabatan?";
                    //if (_idJabatan != "" && _idJabatan != null) _url += $"idjabatan={_idJabatan}&";
                    //if (_startJDate != "" && _startJDate != null) _url += $"startjoindate={_startJDate}&";
                    //if (_endJDate != "" && _endJDate != null) _url += $"endjoindate={_endJDate}&";
                    //if (_status != "" && _status != null) _url += $"status={_status}";

                    ////Set Basic Auth
                    //var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    //var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    //var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    //var getData = client.GetAsync(_url);
                    //getData.Wait();

                    //var dataResult = getData.Result;
                    //if (dataResult.IsSuccessStatusCode)
                    {
                        //var Res = dataResult.Content.ReadAsStringAsync().Result;
                        //listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);
                        listKaryawan = _context.OrderHeds.ToList();

                        foreach (var item in listKaryawan)
                        {
                            OrderHed kr = new OrderHed();
                            kr.OrderNum = item.OrderNum;
                            kr.CustID = item.CustID;
                            kr.CustName = item.CustName;
                            kr.PONum = item.PONum;
                            kr.OrderDate = item.OrderDate;
                            kr.NeedByDate = item.NeedByDate;
                            listOrArray.Add(kr);
                        }
                        listval.Add("Success");
                        listval.Add(listOrArray);
                        return listval;

                    }
                    listval.Add("Not Success");
                    //listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                    return listval;
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }

        }

        public List<OrderHed> DeleteOrderHed(int OrderNum)
        {
            List<OrderHed> val = new List<OrderHed>();
            string Message = string.Empty;
            string sql = "DELETE from OrderHed WHERE OrderNum = '"+OrderNum+"'";
            string sql_2 = "DELETE from OrderDtl WHERE OrderNum = '" + OrderNum + "'";
            ExeQueryCustomer(sql);
            ExeQueryCustomer(sql_2);
            val.Add(new OrderHed());
            return val;
        }

        public List<OrderHed> UpdateOrderHed(OrderHed order)
        {
            List<OrderHed> val = new List<OrderHed>();
            string Message = string.Empty;

            string sql = "UPDATE OrderHed " +
                                "SET CustID = '" + order.CustID + "', " +
                                    "CustName = '" + order.CustName + "', " +
                                    "PONum = '" + order.PONum + "', " +
                                    "OrderDate = '" + order.OrderDate + "', " +
                                    "NeedByDate = '" + order.NeedByDate + "'" +
                                "WHERE OrderNum = '" + order.OrderNum + "'";
            Message = ExeQueryCustomer(sql);
            val.Add(new OrderHed());
            return val;
        }

        public List<OrderDtl> DeleteOrderDetail(int OrderNum, int OrderLine)
        {
            List<OrderDtl> val = new List<OrderDtl>();
            string Message = string.Empty;
            string sql = "DELETE from OrderDtl WHERE OrderNum = '" + OrderNum + "' AND OrderLine = '"+ OrderLine +"'";
            ExeQueryCustomer(sql);
            val.Add(new OrderDtl());
            return val;
        }

    }   
}