﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerPortal.Helper;
using CustomerPortal.Models;
using System.Data;
using System.Web.Mvc;

namespace CustomerPortal.Models
{
    public class PartNumRepository : SQLConnect
    {
        private PartNumRepository() { }
        private static PartNumRepository instance = null;
        public static PartNumRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PartNumRepository();
                }

                return instance;
            }
        }

        public List<PartNumber> GetListPartNum()
        {
            string sql = "Select PartNum, PartDescription, ClassID,IUM,TypeCode, ProdCode,UnitPrice from erp.Part " +
                "           where Company = 'EPIC06' and WebPart = 1";
            return GetDataTable(sql).ToList<PartNumber>();
        }
    }
}