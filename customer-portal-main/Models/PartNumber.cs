﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class PartNumber
    {
        public string PartNum { get; set; }
        public string PartDescription { get; set; }
        public string ClassID { get; set; }
        public string IUM { get; set; }
        public string TypeCode { get; set; }
        public string ProdCode { get; set; }
        public decimal UnitPrice { get; set; }
    }
}