﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Windows;
using Erp.BO;
using Erp.Contracts;
using Erp.Adapters;
using Erp.Proxy.BO;
using Erp.Tablesets;

using Ice;
using Ice.Core;
using Ice.Lib.Framework;

namespace CustomerPortal.Models
{
    public class SalesOrderRepository
    {
        private SalesOrderRepository() { }
        private static SalesOrderRepository instance = null;
        DBCustomerPortalEntities _context = new DBCustomerPortalEntities();

        public static SalesOrderRepository Instance
        {
            get
            {
                if (instance == null) { instance = new SalesOrderRepository(); }

                return instance;
            }
        }

        public Session getConnection(string Username, string Password)
        {
            string User = Username;
            string Pass = Password;
            string Config = Properties.Settings.Default.AppConfig;
            string Location = Properties.Settings.Default.AppLocation;
            Session epiSession = new Ice.Core.Session(User, Pass, Config, Session.LicenseType.Default, @Location);
            return epiSession;
        }

        public string InsertSalesOrder(string Username, string Password)
        {
            string val = "";
            try
            {
                using (Session epiSession = this.getConnection(Username, Password))
                {
                    SalesOrderImpl svcSalesOrder = WCFServiceSupport.CreateImpl<SalesOrderImpl>(epiSession, Epicor.ServiceModel.Channels.ImplBase<SalesOrderSvcContract>.UriPath);
                    SalesOrderDataSet dsSalesOrder = new SalesOrderDataSet();
                    string Company = epiSession.CompanyID;

                    var orderHd = (from oh in _context.OrderHeds where oh.Synced == true select oh).ToList();
                    if (orderHd.Count > 0)
                    {
                        foreach (var hd in orderHd)
                        {
                            int orderNum = 0;
                            int custNum = 0;

                            dsSalesOrder = new SalesOrderDataSet();
                            svcSalesOrder.GetNewOrderHed(dsSalesOrder);

                            string poNumber = hd.PONum;

                            string iCustID = "ADDISON", cCreditLimitMessage = "", cAgingMessage = "";

                            bool lContinue;
                            //dsSalesOrder.OrderHed[0].RowMod = "A";
                            svcSalesOrder.OnChangeofSoldToCreditCheck(0, iCustID, out cCreditLimitMessage, out cAgingMessage, out lContinue, dsSalesOrder);

                            //dsSalesOrder.OrderHed[0].RowMod = "A";
                            svcSalesOrder.ChangeSoldToID(dsSalesOrder);

                            //dsSalesOrder.OrderHed[0].RowMod = "A";
                            svcSalesOrder.ChangeCustomer(dsSalesOrder);

                            bool lCheckForOrderChangedMsg = true, lcheckForResponse = true, lweLicensed = true;
                            string cTableName = "OrderHed", cResponseMsg = "", cCreditShipAction = "", cDisplayMsg = "", cCompliantMsg = "", cResponseMsgOrdRel = "";
                            int iCustNum = 2, iOrderNum = 0;

                            dsSalesOrder.OrderHed[0].PONum = poNumber;
                            dsSalesOrder.OrderHed[0].RowMod = "A";
                            svcSalesOrder.MasterUpdate(lCheckForOrderChangedMsg, lcheckForResponse, cTableName, iCustNum, iOrderNum,
                                lweLicensed, out lContinue, out cResponseMsg, out cCreditShipAction, out cDisplayMsg, out cCompliantMsg,
                                out cResponseMsgOrdRel, out cAgingMessage, dsSalesOrder);

                            orderNum = dsSalesOrder.OrderHed[0].OrderNum;
                            custNum = dsSalesOrder.OrderHed[0].CustNum;

                            int i = 0;
                            var Order = (from oh in _context.OrderDtls where oh.OrderNum == hd.OrderNum select oh).ToList();
                            foreach (var dt in Order)
                            {
                                dsSalesOrder = svcSalesOrder.GetByID(orderNum);
                                svcSalesOrder.InvoiceExists(orderNum);
                                svcSalesOrder.GetNewOrderDtl(dsSalesOrder, orderNum);

                                string partNum = "001MP", uomCode = "", cDeleteComponentsMessage = "", questionString = "", cWarningMessage = "",
                                    cConfigPartMessage = "", cSubPartMessage = "", explodeBOMerrMessage = "", cMsgType = "";
                                Guid SysRowID = new Guid();
                                bool lSubstitutePartExist = false, lIsPhantom = false, multipleMatch = false, promptToExplodeBOM = false,
                                    multiSubsAvail = false, runOutQtyAvail = false;

                                svcSalesOrder.ChangePartNumMaster(ref partNum, ref lSubstitutePartExist, ref lIsPhantom, ref uomCode, SysRowID, "", false, false,
                                    false, true, true, true, out cDeleteComponentsMessage, out questionString, out cWarningMessage,
                                    out multipleMatch, out promptToExplodeBOM, out cConfigPartMessage, out cSubPartMessage, out explodeBOMerrMessage, out cMsgType,
                                    out multiSubsAvail, out runOutQtyAvail, dsSalesOrder);

                                dsSalesOrder.OrderDtl[i].RowMod = "A";
                                decimal ipSellingQuantity = 2;
                                string pcMessage = "", pcNeqQtyAction = "", opWarningMsg = "", cSellingQuantityChangedMsgText = "";
                                svcSalesOrder.ChangeSellingQtyMaster(dsSalesOrder, ipSellingQuantity, false, false, true, true, false, true, partNum, "", "", "", 0,
                                    "EA", 1, out pcMessage, out pcNeqQtyAction, out opWarningMsg, out cSellingQuantityChangedMsgText);

                                dsSalesOrder.OrderDtl[i].RowMod = "A";
                                iOrderNum = orderNum;
                                iCustNum = custNum;
                                svcSalesOrder.Update(dsSalesOrder);
                                /*svcSalesOrder.MasterUpdate(lCheckForOrderChangedMsg, lcheckForResponse, cTableName, iCustNum, iOrderNum,
                                    lweLicensed, out lContinue, out cResponseMsg, out cCreditShipAction, out cDisplayMsg, out cCompliantMsg,
                                    out cResponseMsgOrdRel, out cAgingMessage, dsSalesOrder);*/
                                i++;
                            }
                        }
                    }
                        val = "Sync completed successfully!";
                }
            }
            catch (Exception e)
            {
                val = e.Message;
            }

            return val;
        }
    }
}