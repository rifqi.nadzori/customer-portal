﻿using CustomerPortal.Helper;
using CustomerPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace CustomerPortal.Repository
{
    public class GetEpicorDataRepo : SQLConnect
    {
        private GetEpicorDataRepo() { }
        private static GetEpicorDataRepo instance = null;
        public static GetEpicorDataRepo Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GetEpicorDataRepo();
                }

                return instance;
            }
        }

        public List<InvoiceModelEpicor> GetAllDatas(string CustID, string Company)
        {
            int varCustNum = 0;
           // List<InvoiceModelEpicor> data = GetEpicorDataRepo.Instance.GetCustNum(Session["CustID"].ToString(), Session["Company"].ToString());
            List<InvoiceModelEpicor> data = GetEpicorDataRepo.Instance.GetCustNum(CustID, Company);
            if (data.Count > 0)
            {
                foreach (var r in data)
                {
                    varCustNum = r.CustNum;
                }
            }
            string query = "SELECT InvoiceNum,OrderNum, c.CustID, c.Name,InvoiceDate, PONum,InvoiceAmt,DocInvoiceAmt,i.CustNum FROM Erp.InvcHead i JOIN Customer c ON c.CustNum = i.CustNum and c.Company = i.Company WHERE  i.Company ='" + Company + "' and i.CustNum =" + varCustNum;
            return GetDataTable(query).ToList<InvoiceModelEpicor>();
        }

        public List<InvoiceModelEpicor> GetCustNum(string custID, string company)
        {
            //custID = "ADDISON";
            string query = "SELECT CustNum FROM Erp.Customer WHERE Company = '" + company + "' and CustID = '" + custID + "'";
            return GetDataTable(query).ToList<InvoiceModelEpicor>();
        }

        public List<CustIdListModel> GetListCustID()
        {
            string query = "select CustID,Name from Erp.Customer where  Company = 'EPIC06'";
            return GetDataTable(query).ToList<CustIdListModel>();
        }


    }
}